# HAL-9000
# Copyright (C) 2020  Cameron Himes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sqlite3
import hashlib
import random

TABLENAME_CHAIRS = "CHAIRS"
TABLENAME_ADMINS = "ADMINS"


class SQLEngine(object):
    # make object for database io
    def __init__(self, datapath):
        # save path to data and table name
        self.datapath = datapath
        # open connection
        self.establishConnection()

    # initialize database
    def establishConnection(self):
        # open connection to database file
        self.connection = sqlite3.connect(self.datapath)
        # get a cursor
        self.cursor = self.connection.cursor()
        # create chair table if needed
        self.cursor.execute("create table if not exists {table} (user text, count integer)".format(
            table=TABLENAME_CHAIRS))
        # create admin table if needed
        self.cursor.execute("create table if not exists {table} (user text, authorized integer, code text)".format(
            table=TABLENAME_ADMINS))

    # save all changes and close the database
    def closeConnection(self):
        # close the cursor
        self.cursor.close()
        # save any uncommitted changes
        self.connection.commit()
        # close the connection
        self.connection.close()

    #   ____  _             _
    #  / ___|| |__    __ _ (_) _ __
    # | |    | '_ \  / _` || || '__|
    # | |___ | | | || (_| || || |
    #  \____||_| |_| \__,_||_||_|
    #

    # increment chair counter for user
    def incrementChair(self, user):
        # check if user already exists
        self.cursor.execute(
            "SELECT * FROM {table} WHERE user=?".format(table=TABLENAME_CHAIRS), (user,))
        # get reply
        reply = self.cursor.fetchone()
        if reply == None:
            # make a new user in the table
            self.cursor.execute("INSERT INTO {table} VALUES (?,?)".format(
                table=TABLENAME_CHAIRS), (user, 1))
        else:
            # increment current user
            username, count = reply
            self.cursor.execute("UPDATE {table} SET count=? WHERE user=?".format(
                table=TABLENAME_CHAIRS), (count+1, username))
        # save changes
        self.connection.commit()

    # get chair count for a user
    def getChairCount(self, user):
        # get user info
        self.cursor.execute(
            "SELECT * FROM {table} WHERE user=?".format(table=TABLENAME_CHAIRS), (user,))
        # get reply
        reply = self.cursor.fetchone()
        # check if user exists
        if reply == None:
            return -1
        else:
            # get chair count
            _, count = reply
            return count

    # get the top 5 chair users
    def getTop5ChairThrowers(self):
        # get user info
        self.cursor.execute(
            "SELECT * FROM {table} ORDER BY count DESC".format(table=TABLENAME_CHAIRS))
        # get reply
        reply = self.cursor.fetchmany(5)
        # make return array
        userdata = []
        # process entries
        for r in reply:
            # add user data
            username, count = r
            userdata.append([username, count])
        # return list of users
        return userdata

    #     _        _             _
    #    / \    __| | _ __ ___  (_) _ __
    #   / _ \  / _` || '_ ` _ \ | || '_ \
    #  / ___ \| (_| || | | | | || || | | |
    # /_/   \_\\__,_||_| |_| |_||_||_| |_|
    #

    # check if user exists
    def __isInsideAdminDB__(self, userid):
        # search for user
        self.cursor.execute(
            "SELECT * FROM {table} WHERE user=?".format(table=TABLENAME_ADMINS), (userid,))
        reply = self.cursor.fetchone()
        # return true if userid is in the admin table
        # NOTE: there will never be a second entry for the same user id
        return reply != None

    # check if user is an admin
    def isAdmin(self, userid):
        # search for user
        self.cursor.execute(
            "SELECT * FROM {table} WHERE user=? AND authorized=?".format(table=TABLENAME_ADMINS), (userid, 1))
        reply = self.cursor.fetchone()
        # return true if userid is authorized
        return reply != None

    # register admin
    def registerAdmin(self, userid):
        # generate auth code
        hashCode = hashlib.sha512()
        hashCode.update(userid.encode())
        hashCode.update(str(random.random()).encode())
        authCode = hashCode.hexdigest()
        # add to database (overwrite auth code if user exists)
        if self.__isInsideAdminDB__(userid):
            self.cursor.execute("UPDATE {table} SET code=? WHERE user=?".format(
                table=TABLENAME_ADMINS), (authCode, userid))
        else:
            self.cursor.execute("INSERT INTO {table} VALUES (?,?,?)".format(
                table=TABLENAME_ADMINS), (userid, 0, authCode))
        # save changes
        self.connection.commit()
        # return auth code
        return authCode

    # verify admin
    def verifyAdmin(self, userid, authCode):
        # make container for status
        status = False
        # check if user exists
        if self.__isInsideAdminDB__(userid):
            # get user info
            self.cursor.execute(
                "SELECT * FROM {table} WHERE user=?".format(table=TABLENAME_ADMINS), (userid,))
            _, auth, code = self.cursor.fetchone()
            # report success if user was previously authorized
            if auth == 1:
                return True
            # attempt to authorize
            if auth == 0 and code == authCode:
                self.cursor.execute("UPDATE {table} SET authorized=? WHERE user=?".format(
                    table=TABLENAME_ADMINS), (1, userid))
                status = True
        # return true if user is successfully authorized
        return status

from bs4 import BeautifulSoup
from urllib.request import urlopen

MEETUP_URL = "https://www.meetup.com/High-Country-Linux-Users-Group-HiLUG/events/"


def _getMeetingURL_(url):
    # get page HTML
    with urlopen(url) as fp:
        bs = BeautifulSoup(fp, features="lxml")
    # register the status code
    # NOTE: 0=success, 1=failure
    status = 0
    # get the needed div
    url = bs.find(class_="eventCard--clickable")
    # get the link tag
    if url == None:
        url = "ERROR! No class named eventCard--clickable."
        status = 1
    else:
        url = url.find("a")
    # check the link tag for a meeting URL
    if status == 0:
        if url == None:
            url = "ERROR! No <a> tag inside eventCard--clickable."
            status = 1
        else:
            url = url.get("href")
    # resolve the full URL
    if status == 0:
        if url == None:
            url = "ERROR! No href inside eventCard--clickable."
            status = 1
        else:
            url = "https://www.meetup.com" + url
    # return the complete URL
    return url, status


def _parseInformation_(url):
    # get page HTML
    with urlopen(url) as fp:
        bs = BeautifulSoup(fp, features="lxml")
    # title
    title = bs.find(class_="pageHead-headline")
    if title == None:
        title = "ERROR!"
    else:
        title = title.get_text()
    # time
    time = bs.find(class_="eventTimeDisplay-startDate-time")
    if time == None:
        time = "ERROR!"
    else:
        time = time.get_text()
    # date
    date = bs.find(class_="eventTimeDisplay-startDate")
    if date == None:
        date = "ERROR!"
    else:
        date = date.find("span")
        if date == None:
            date = "ERROR!"
        else:
            date = date.get_text()
    # location
    location = bs.find("address")
    if location == None:
        location = "ERROR!"
    else:
        streetAddress = location.find(class_="venueDisplay-venue-address")
        if streetAddress == None:
            streetAddress = "ERROR!"
        else:
            streetAddress = streetAddress.get_text()
        placeName = location.find(class_="wrap--singleLine--truncate")
        if placeName == None:
            placeName = "ERROR!"
        else:
            placeName = placeName.get_text()
        location = "%s (%s)" % (placeName, streetAddress)
    return title, time, date, location


def _parseTimestamp_(url):
    # get page HTML
    with urlopen(url) as fp:
        bs = BeautifulSoup(fp, features="lxml")
    # timestamp
    timestamp = bs.find(class_="eventTimeDisplay")
    if timestamp == None:
        timestamp = -1
    else:
        timestamp = timestamp.find("time")
        if timestamp == None:
            timestamp = -1
        else:
            timestamp = timestamp.get("datetime")
            if timestamp == None:
                timestamp = -1
            else:
                timestamp = int(timestamp)
    return timestamp


def getInformation():
    url, status = _getMeetingURL_(MEETUP_URL)
    if status == 0:
        title, time, date, location = _parseInformation_(url)
        return "Title: %s\nDate: %s\nTime: %s\nLocation: %s\nURL: %s" % (title, date, time, location, url)
    else:
        return "The meeting URL could not be loaded."


def getTimestamp():
    url, status = _getMeetingURL_(MEETUP_URL)
    if status == 0:
        timestamp = _parseTimestamp_(url)
        return timestamp
    else:
        return -1


def getURL():
    url, status = _getMeetingURL_(MEETUP_URL)
    if status == 0:
        return url
    else:
        return "The meeting URL could not be loaded."


def __test__():
    # Write some pre-test information
    print("[NOTE] Test method was called for MeetingToolkit.py")
    print("[INFO] Using URL: {url}".format(url=MEETUP_URL))

    # Check the webscrape functions for errors
    print("[INFO] === PERFORM WEBSCRAPE TESTS ===")

    # TEST 1: Get the next meeting URL
    print("[TEST] Calling _getMeetingURL_()...")
    response = _getMeetingURL_(MEETUP_URL)
    print("[INFO] Response: {response}".format(response=response))

    # Check if URL is valid before using it for more tests
    targetURL, status = response
    if status != 0:
        print("[FAIL] Can not continue testing with bad URL")
        exit()
    else:
        print("[NOTE] This response will be used for future tests")

    # TEST 2: Get the next meeting time
    print("[TEST] Calling _parseTimestamp_()...")
    response = _parseTimestamp_(targetURL)
    print("[INFO] Response: {response}".format(response=response))

    # TEST 3: Get the next meeting information
    print("[TEST] Calling _parseInformation_()...")
    response = _parseInformation_(targetURL)
    print("[INFO] Response: {response}".format(response=response))

    # Check HAL 9000's API for errors
    print("[INFO] === PERFORM HAL 9000 TESTS ===")

    # TEST 1: Get the next meeting URL
    print("[TEST] Calling getURL()...")
    response = getURL()
    print("[INFO] Response: {response}".format(response=response))

    # TEST 2: Get the next meeting time
    print("[TEST] Calling getTimestamp()...")
    response = getTimestamp()
    print("[INFO] Response: {response}".format(response=response))

    # TEST 3: Get the next meeting information
    print("[TEST] Calling getInformation()...")
    response = getInformation()
    print("[INFO] Response: {response}".format(response=response))

    # Complete testing
    print("[INFO] === ALL TESTS COMPLETE ===")


if __name__ == "__main__":
    print("[WARN] This file is a helper class for HAL 9000. Running in standalone mode is unsupported and git issues will be ignored.")
    __test__()
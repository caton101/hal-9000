# HAL-9000
# Copyright (C) 2020  Cameron Himes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import subprocess
import slack
import asyncio
import time
from random import choice
from SQLEngine import SQLEngine
import MeetingToolkit

loop = asyncio.get_event_loop()

TIME_LAST_SCAN = time.time()
WAIT_TIME = 10
CALLSIGN = "."

ADMIN_COMMAND_LIST = """Admin Commands:
`.acheck @user`: check if the mentioned user is an admin
`.ahelp`: show this message
`.aregister`: register for admin privleges
`.arestart`: restart HAL-9000
`.astop`: stop HAL-9000
`.averify`: verify your admin code
"""

USER_COMMAND_LIST = """User Commands:
`.aregister`: register for admin privleges
`.butterflies`: show the "Real Programmers" comic from XKCD
`.butterfly`: show the "Real Programmers" comic from XKCD
`.chair @user`: throw a chair at the mentioned user
`.chairtop`: list the top 5 chair throwers
`.droplet`: show connection information about the HiLUG droplet server
`.gitdiff`: show any modifications after the current commit
`.gitrepo`: show a link to the HAL-9000 GitLab repo
`.gitver`: show the hash of the current commit
`.help`: show this message
`.hissyfit`: show an image of a hissyfit
`.linux`: show a Linux-related gif
`.meeting`: show information about the next meeting
`.meetup`: show a link to the HiLUG Meetup page
`.ping`: pong!
`.test`: checks if HAL can see your command
`.website`: show a link to the official HiLUG website
"""

print("Starting Slack connection...")
client = slack.WebClient(
    token=os.environ['SLACK_API_TOKEN'],
    run_async=True
)

print("Loading chair engine...")
se = SQLEngine("./slack.db")

print("Getting channel list...")
channels = [[x["id"], x["name"]] for x in loop.run_until_complete(
    client.conversations_list()).data["channels"]]
print("Setting channel timestamps...")
channelClock = [time.time() for _ in range(len(channels))]
print("Getting user list...")
users = []
for x in loop.run_until_complete(client.users_list()).data["members"]:
    if len(x['profile']['display_name']) > 0:
        users.append([x['id'], x['profile']['display_name']])
    else:
        users.append([x['id'], x['name']])


print("Joining channels...")
for channel in channels:
    try:
        print("\tTrying to join %s..." % channel[1])
        loop.run_until_complete(client.conversations_join(channel=channel[0]))
    except:
        continue


def userNameLookup(userID):
    for user in users:
        if user[0] == userID:
            return user[1]
    return "!LOOKUP_FAILED"


def userIDLookup(userName):
    for user in users:
        if user[1] == userName:
            return user[0]
    return "!LOOKUP_FAILED"


def channelNameLookup(channelID):
    for channel in channels:
        if channel[0] == channelID:
            return channel[1]
    return "!LOOKUP_FAILED"


def channelIDLookup(channelName):
    for channel in channels:
        if channel[1] == channelName:
            return channel[0]
    return "!LOOKUP_FAILED"


def writeMessage(channelName, message):
    loop.run_until_complete(client.chat_postMessage(
        channel=channelName,
        text=message
    ))


def parseCommand(message, channelName):
    text = message['text'].split()
    print("Attempting to execute command `%s`..." % text[0])
    if text[0] == ".test":
        writeMessage(channelName, "Hello %s, I see your command." %
                     userNameLookup(message['user']))
        print("Executed command `.test`.")
    elif text[0] == ".ahelp":
        if se.isAdmin(message['user']):
            writeMessage(
                channelName, ADMIN_COMMAND_LIST)
        else:
            writeMessage(channelName, "{username}, you are not authorized to run this command.".format(
                username=userNameLookup(message['user'])))
        print("Executed command `.ahelp`.")
    elif text[0] == ".arestart":
        if se.isAdmin(message['user']):
            writeMessage(
                channelName, "Restarting HAL. This may take a few seconds.")
            print("Restarting HAL...")
            stop(69)
        else:
            writeMessage(channelName, "{username}, you are not authorized to run this command.".format(
                username=userNameLookup(message['user'])))
        print("Executed command `.arestart`.")
    elif text[0] == ".astop":
        if se.isAdmin(message['user']):
            writeMessage(channelName, "Stopping HAL.")
            print("Stopping HAL...")
            stop(0)
        else:
            writeMessage(channelName, "{username}, you are not authorized to run this command.".format(
                username=userNameLookup(message['user'])))
        print("Executed command `.astop`.")
    elif text[0] == ".acheck":
        if se.isAdmin(message['user']):
            if len(text) == 2:
                target = text[1][2:len(text[1])-1]
                if se.isAdmin(target):
                    writeMessage(channelName, "{username} is an admin.".format(
                        username=userNameLookup(target)))
                else:
                    writeMessage(channelName, "{username} is not an admin.".format(
                        username=userNameLookup(target)))
            else:
                writeMessage(channelName, "Usage: `.acheck @user`")
        else:
            writeMessage(channelName, "{username}, you are not authorized to run this command.".format(
                username=userNameLookup(message['user'])))
        print("Executed command `.acheck`.")
    elif text[0] == ".aregister":
        userID = message['user']
        userName = userNameLookup(userID)
        code = se.registerAdmin(userID)
        print("\n\nA user has requested admin privleges.")
        print("\t\tUsername: {0}".format(userID))
        print("\t\tUserid: {0}".format(userName))
        print("\t\tConfirmation Code: {0}".format(code))
        print("\n\n")
        writeMessage(channelName, "{username} has been added to the admin database. Retrieve your code from the terminal output and use `.averify <code>` to gain admin privleges.".format(
            username=userName))
        print("Executed command `.aregister`.")
    elif text[0] == ".averify":
        userID = message['user']
        userName = userNameLookup(userID)
        if len(text) == 2:
            code = text[1]
            result = se.verifyAdmin(userID, code)
            if result:
                writeMessage(channelName, "Congratulations, {username}. You have successfully authorized yourself. Use `.ahelp` to see admin commands.".format(
                    username=userName))
            else:
                writeMessage(channelName, "Sorry, {username}. You failed to authorize yourself. Please check your code or try again.".format(
                    username=userName))
        else:
            writeMessage(channelName, "Usage: .averify <code>")
        print("Executed command `.averify`.")
    elif text[0] == ".gitrepo":
        writeMessage(channelName, "https://gitlab.com/caton101/hal-9000/")
        print("Executed command `.gitrepo`.")
    elif text[0] == ".ping":
        writeMessage(channelName, "Pong!")
        print("Executed command `.ping`.")
    elif text[0] == ".gitver":
        commit = subprocess.run(
            ['sh', '-c', 'git log | head --lines 1'], stdout=subprocess.PIPE).stdout.decode()
        writeMessage(channelName, "I am running %s." % commit[:len(commit)-1])
        print("Executed command `.gitver`.")
    elif text[0] == ".help":
        writeMessage(channelName, USER_COMMAND_LIST)
        print("Executed command `.help`.")
    elif text[0] == ".droplet":
        writeMessage(channelName, "You can connect to the droplet server using `ssh -p 6177 user@165.227.222.21`. Talk to Cameron if you want an account.")
    elif text[0] == ".chair":
        # list of chair models from https://en.wikipedia.org/wiki/List_of_chairs
        chairs = [
            "10 Downing Street Guard Chair",
            "Forty-In-Four stacking chair",
            "Aalto armchair",
            "Adirondack chair",
            "Aeron chair",
            "Air chair",
            "Armchair",
            "Bachelor's chair",
            "Balans chair",
            "Ball chair",
            "Bar stool chair",
            "Barcelona chair",
            "Bardic chair",
            "Barrel chair",
            "Bath chair",
            "Beach chair",
            "Sacco chair",
            "Bean bag chair",
            "Bench",
            "Bergère",
            "Bikini chair",
            "Bofinger chair",
            "Bosun's chair",
            "Breuer Chair",
            "Brewster Chair",
            "Bubble Chair",
            "Bungee chair",
            "Butterfly chair",
            "Campeche chair",
            "Cantilever chair",
            "Captain's chair",
            "Caquetoire",
            "Car chair",
            "Carver chair",
            "Cathedra",
            "Chaise a bureau chair",
            "Chaise longue",
            "Chesterfield chair",
            "Chiavari chair",
            "Club chair",
            "Cogswell chair",
            "Corner chair",
            "Curule chair",
            "Dante chair",
            "Deck chair",
            "Dentist chair",
            "Dining chair",
            "Director's chair",
            "Easy chair",
            "Eames Lounge Chair",
            "Egg chair",
            "Electric chair",
            "Elijah's chair",
            "Emeco 1006 chair",
            "Farthingale chair",
            "Fauteuil",
            "Fiddleback chair",
            "Fighting chair",
            "Folding chair",
            "Folding seat",
            "Friendship bench",
            "Gaming chair",
            "Garden Egg chair",
            "Glastonbury chair",
            "Glider",
            "Hassock",
            "High chair",
            "Hanging Egg Chair",
            "Inflatable chair",
            "Ironing chair",
            "Jack and Jill",
            "Jump seat",
            "Kneeling chairs",
            "Knotted chair",
            "Ladderback chair",
            "Lambing chair",
            "Lawn chair",
            "Lifeguard chair",
            "Lift chair",
            "Litter",
            "Louis Ghost chair",
            "Massage chair",
            "Monobloc chair",
            "Morris chair",
            "Muskoka chair",
            "Navy chair",
            "No. 14 chair",
            "Nursing chair",
            "Office chair",
            "Orbiter",
            "ON Chair",
            "Ottoman",
            "Ovalia Egg Chair",
            "Onit chair",
            "Panton Chair",
            "Papasan chair",
            "Parsons chair",
            "Pop",
            "Patio chair",
            "Peacock chair",
            "Pew",
            "Pew stacker chair",
            "Planter's chair",
            "Poäng",
            "Poofbag chair",
            "Porter's chair",
            "Potty chair",
            "Portuguese chair",
            "Pouffe",
            "Power chairs",
            "Pressback chair",
            "Stroller",
            "Recliner",
            "Red and Blue Chair",
            "Restraint chair",
            "Revolving chair",
            "Rex chair",
            "Ribbon Chair",
            "Rocking chair",
            "Rover chair",
            "Rumble seat",
            "Saddle chair",
            "Savonarola chair",
            "Sedan chair",
            "Sgabello",
            "Shaker rocker",
            "Shaker tilting chair",
            "Shower chair",
            "Side chair",
            "Sit-stand chair",
            "Sling chair",
            "Slumber chair",
            "Spinning chair",
            "Stacking chair",
            "Steno chair",
            "Step chair",
            "Stool",
            "Sweetheart chair",
            "Swivel chairs",
            "Tantra Chair",
            "Tarachair",
            "Tête-à-tête chair",
            "Throne",
            "Toilet chair",
            "Tuffet",
            "Tulip chair",
            "Turned chair",
            "two-slat post-and-rung shaving chair",
            "Reception chair",
            "Voyeuse chair",
            "Wainscot Chair",
            "Watchman's chair",
            "Wassily Chair",
            "Wheelchair",
            "Wicker chair",
            "Wiggle chair",
            "Windsor chair",
            "Wing chair",
            "Writing armchair",
            "X-chair",
            "Zaisu",
            "Zig-Zag Chair",
            "Zero-gravity chair"
        ]
        chosenChair = choice(chairs)
        article = "an" if chosenChair[0].lower() in "aeiou" else "a"
        se.incrementChair(message['user'])
        if len(text) == 1:
            writeMessage(channelName, "%s threw %s _%s_." %
                         (userNameLookup(message['user']), article, chosenChair))
        else:
            writeMessage(channelName, "%s threw %s _%s_ at %s." % (userNameLookup(
                message['user']), article, chosenChair, userNameLookup(text[1][2:len(text[1])-1])))
        print("Executed command `.chair`.")
    elif text[0] == ".chairtop":
        output = "Rank - Username - Chairs"
        n = 1
        for u in se.getTop5ChairThrowers():
            output += "\n#%d %s (%d chairs)" % (n, userNameLookup(u[0]), u[1])
            n += 1
        writeMessage(channelName, output)
        print("Executed command `.chairtop`.")
    elif text[0] == ".linux":
        URLS = [
            "https://tenor.com/view/rick-ashtley-never-gonna-give-up-rick-roll-gif-4819894",
            "https://tenor.com/view/inquisition-monty-python-spanish-inquisition-walk-in-gif-17117394",
            "https://tenor.com/view/peanuts-dance-gif-4018568",
            "https://tenor.com/view/cloud-tux-linux-penguin-gif-13909136",
            "https://tenor.com/view/penguin-happy-jump-linux-users-linux-gif-13909131",
            "https://tenor.com/view/richard-stallman-triggered-oboi-warnedem-gif-15656329",
            "https://tenor.com/view/stallman-meme-extreme-gif-15311412",
            "https://tenor.com/view/linux-penguin-kernel-os-gif-17138994",
            "https://tenor.com/view/linux-ubuntu-software-gif-13909128",
            "https://tenor.com/view/dark-kissing-linux-wife-software-gif-17187287",
            "https://tenor.com/view/koala-linux-destroy-gif-16065231",
            "https://tenor.com/view/linux-salva-sudo-rm-troll-gif-16293992",
            "https://tenor.com/view/%d0%ba%d1%80%d0%b0%d1%81%d0%be%d1%82%d0%b8%d1%89%d0%b0%d1%82%d0%be%d0%ba%d0%b0%d0%ba%d0%b0%d1%8f-code-hacks-gif-15028875",
            "https://tenor.com/view/mad-linux-pc-smash-destroy-gif-17183877",
            "https://tenor.com/view/linux-linus-linus-torvalds-middle-finger-fuck-off-gif-16334894",
            "https://tenor.com/view/manjaro-manjaro-yes-manjaro-linux-yes-gif-14068816",
            "https://tenor.com/view/gentoo-linux-kernel-gentoo-linux-nigga-gif-14453867",
            "https://tenor.com/view/ubuntu-ubuntu-linux-kernel-linux-not-windows-gif-14453921",
            "https://tenor.com/view/ubuntu-who-doth-summon-the-mighty-ubuntu-software-gif-16821427",
            "https://gph.is/2qcL5Va",
            "https://gph.is/g/Z2nKMq5",
            "https://gph.is/g/am8nl5E",
            "https://gph.is/g/aQepoba",
            "https://gph.is/2gJXxHX",
            "https://gfycat.com/admirablediscreteglobefish",
            "https://gfycat.com/teemingashamedkagu",
            "https://gfycat.com/inconsequentialmenacingbrocketdeer",
            "https://gfycat.com/sociableweecottontail",
            "https://gfycat.com/academichighgalago",
            "https://gfycat.com/adorablepettyanaconda",
            "https://gfycat.com/dimcomplicatedkoala",
            "https://gfycat.com/bigheartedmassiveeastsiberianlaika",
            "https://gfycat.com/quarterlyfirstfireant",
            "https://gfycat.com/mediocrelittlebarnacle",
            "https://gfycat.com/bareforcefuldrever",
            "https://gfycat.com/dentalmediocreattwatersprairiechicken",
            "https://gfycat.com/rewardingfirmbongo",
            "https://gfycat.com/acrobaticadmiredasianlion",
            "https://gfycat.com/sadwigglychrysalis-linux"
        ]
        selectedURL = choice(URLS)
        writeMessage(channelName, "Don't worry, %s. I got your GNU/Linux fix. %s" %
                     (userNameLookup(message['user']), selectedURL))
        print("Executed command `.linux`.")
    elif text[0] == ".butterflies":
        writeMessage(
            channelName, "https://imgs.xkcd.com/comics/real_programmers.png")
        print("Executed command `.butterflies`.")
    elif text[0] == ".butterfly":
        writeMessage(
            channelName, "https://imgs.xkcd.com/comics/real_programmers.png")
        print("Executed command `.butterfly`.")
    elif text[0] == ".meetup":
        writeMessage(
            channelName, "https://www.meetup.com/High-Country-Linux-Users-Group-HiLUG/")
        print("Executed command `.meetup`.")
    elif text[0] == ".website":
        writeMessage(channelName, "https://www.boonelinux.com/")
        print("Executed command `.website`.")
    elif text[0] == ".hissyfit":
        URLS = [
            "https://media.giphy.com/media/13AXYJh2jDt2IE/giphy.gif",
            "https://media.giphy.com/media/LkwssZJSWkdX5P4lc1/giphy.gif",
            "https://media.giphy.com/media/Jqm7Q8vl4erni/giphy.gif",
            "https://media.giphy.com/media/VjpoadtqZaq2s/giphy.gif",
            "https://media.giphy.com/media/niP9quWQpUmk0/giphy.gif",
            "https://media.giphy.com/media/s4T64Kx2p0fhm/giphy.gif",
            "https://media.giphy.com/media/jkYhaCyVaTxqHLk2Yl/giphy.gif",
            "https://media.giphy.com/media/GnqgAJvlpkK76/giphy.gif"
        ]
        writeMessage(channelName, "%s" % (choice(URLS)))
        print("Executed command `.hissyfit`.")
    elif text[0] == ".meeting":
        writeMessage(channelName, MeetingToolkit.getInformation())
        print("Executed command `.meeting`.")
    elif text[0] == ".gitdiff":
        status = subprocess.run(
            ['sh', '-c', 'LANG=en_US.UTF8 git diff'], stdout=subprocess.PIPE).stdout.decode()
        if status == "":
            writeMessage(channelName, "`git diff` did not return any output.")
        else:
            writeMessage(
                channelName, "Here is the output of `git diff`:\n```%s```" % status[:len(status)-1])
        print("Executed command `.gitdiff`.")
    else:
        # NOTE: This was probably an accident and should be treated like a normal message
        print(
            "The command `%s` was probably an accident. It will be treated as a message." % text[0])
        parseMessage(message, channelName)


def parseMessage(message, channelName):
    textRaw = message['text']
    if textRaw.startswith(". "):
        writeMessage(channelName, "Hey, %s. Why are you starting a sentence with a period? I think punctuation should occur after a sentence." %
                     userNameLookup(message['user']))
    elif textRaw.startswith("...") or textRaw.startswith("…"):
        writeMessage(channelName, "Don't forget to take a 30 second pause before reading %s's message. After all, they thought the pause was necessary." %
                     userNameLookup(message['user']))
    elif textRaw.find("...") != -1 or textRaw.find("…") != -1:
        writeMessage(channelName, "Don't forget to take a 30 second pause while reading %s's message. After all, they thought the pause was necessary." %
                     userNameLookup(message['user']))
    elif textRaw.find("..") != -1 or textRaw.find("‥") != -1:
        writeMessage(
            channelName, "Seriously? Two dots. Use three dots like a normal person.")
    elif textRaw.upper().startswith(".NET") or textRaw.upper().find(" .NET ") != -1 or textRaw.upper().endswith(" .NET"):
        writeMessage(
            channelName, "Why don't you use a real framework instead of getting bent over by Microsoft?")
    elif textRaw.upper().find("DUCK") != -1:
        writeMessage(
            channelName, "https://media.giphy.com/media/r6oI6DiZu9Lq0/giphy.gif")
    elif textRaw == "/giphy rob schneider":
        writeMessage(channelName, ".chair @%s" %
                     (userNameLookup(message['user'])))
    elif textRaw.upper().find("FREEDOM") != -1:
        URLS = [
            "https://media.giphy.com/media/wy0p4fpxdhHXy/giphy.gif",
            "https://media.giphy.com/media/pYfEywOAolwnm/giphy.gif",
            "https://media.giphy.com/media/6BiC8e8sypeow/giphy.gif",
            "https://media.giphy.com/media/t1i8KZ7momVs4/giphy.gif",
            "https://media.giphy.com/media/Ez01FtPZuFYVa/giphy.gif"
        ]
        writeMessage(channelName, "%s" % (choice(URLS)))


def stop(exitCode=0):
    se.closeConnection()
    exit(exitCode)


def main():
    global TIME_LAST_SCAN
    while True:
        print("Scanning for messages...")
        indexCounter = 0
        for channel in channels:
            print("\tReading channel %s..." % channel[1])
            try:
                hist = loop.run_until_complete(
                    client.conversations_history(channel=channel[0])).data["messages"]
                hist = sorted(hist, key=lambda x: x['ts'])
                for message in hist:
                    ts = message["ts"]
                    if float(ts) > channelClock[indexCounter] and len(message['text']) > 0:
                        if message['text'][0] == CALLSIGN:
                            try:
                                parseCommand(message, channel[1])
                            except KeyboardInterrupt:
                                stop()
                            except Exception as e:
                                print(e)
                        else:
                            parseMessage(message, channel[1])
                channelClock[indexCounter] = time.time()
                indexCounter += 1
                time.sleep(0.5)
            except KeyboardInterrupt:
                stop()
            except Exception as e:
                print(e)
        print("Scan complete.")
        print("Waiting for messages (%.2f seconds)..." % WAIT_TIME)
        time.sleep(WAIT_TIME)


try:
    main()
except KeyboardInterrupt:
    stop()
